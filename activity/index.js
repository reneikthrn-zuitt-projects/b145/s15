// Hello World
console.log('Hello World!');

// Create a function that will accept the first name, last name and age as arguments and print those details in the console as a single string.
/* 
	USING VARIABLE DECLARATION [ORIGINAL APPROACH]
	let fName = 'John';
	let lName = 'Smith';
	let age = '30';
*/
function fullName(fName, lName, age) {
	console.log(fName + ' ' + lName + ' is ' + age + ' years of age.');
	// console.log('This was printed inside of the function');
}

fullName('John', 'Smith', '30'); // Single String 


// Create another function that will return a value and store it in a variable.

/* 
	ORIGINAL APPROACH:
	function isMarried() {
		return 'The value of isMarried is: true';
	}

	let isItMarried = isMarried();
	console.log(isMarried());
*/
function isMarried() {
	// console.log('This was printed inside of the function');
	return true; // answers a question of yes/no (return a value)
}

let isItMarried = isMarried();
console.log('The value of isMarried is: ' + isItMarried); // stored in a variable
