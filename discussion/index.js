// let's include a confirm message
// console.log() is used to output/display messages in the console of your browser
console.log('Hello from JS');

// error message
console.error('Error Something');

// warning message
console.warn('Ooops, Waarning!');

/*
	Standard Function Structure

	Example Syntax:
		function nameOfFunction() {
			// instructions/procedures
			return;
		}
*/

// Let's create a function that will display an error
function errorMessage() {
	console.error('Error, oh no!! AAHH');
}

errorMessage();


/* FUNCTIONS DISCUSSION [DEC. 6, 2021] */


function greetings(){
	//procedures
	console.log('Salutations from JS');
}

// Invocation
greetings();


/* 
	There are different methods in declaring a function in JavaScript.
	
	Section 1.) Variables declared OUTSIDE a function can be used INSIDE a function.

	Section 2.) "Blocked Scope" variables - this means that variables can only be used within the scope of the function.

	Section 3.) Function with Parameters
	=> PARAMETER - acts as a variable or a container/catchers that only exists inside a function. A parameter is used to store information that is provided to a function when it is called or invoked.

	Section 4.) Functions with Multiple Parameters

	Section 5.) Using variables as Arguments

	Section 6.) The RETURN Statement

	Section 7.) Using Functions as Arguments

*/


// ----- SECTION 1 -----
let givenName = 'John';
let familyName = 'Smith';

// let's try to create a function that will utilize the information outside it's scope
function fullName(){
	// combine the values the information outside and display it inside the console
	console.log(givenName + ' ' + familyName);
}
/*
	Even if variables are declared HERE. It will still work.
	let givenName = 'John';
	let familyName = 'Smith';
*/

// Invoking/Calling out functions we do it, by adding parenthesis() after the function name.
fullName();


// ----- SECTION 2 -----
// let's crete a function that will allow us to compute the values of 2 variables.
function computeTotal() {
	let numA = 20;
	let numB = 5;
	// add the values and display it inside the console
	console.log(numA + numB);
}

// console.log(numA); -- error => the variable cannot be used outside the sccope of the function where it was declared.
// call out the function
computeTotal();


// ----- SECTION 3 -----
// let's create a function that emulate a pokemon battle
function pokemon(pangalan){
	// we're going to use the "parameter" declared on this function to be processed and displayed inside the console.
	console.log('I choose you: ' + pangalan);
}

// Invocation
pokemon('Pikachu');

/* 
	PARAMETERS VS. ARGUMENTS
	Parameters 
		-> placed in declaration

	Arguments 
		-> placed in invocation
		-> is the 'ACTUAL' value that is provided inside a function for it to work properly. The TERM argument is used when functions are called/invoked as compared to parameters when a function is simply declared.
*/


// ----- SECTION 4 -----
// let's declare a function that will get the sum of multiple values
function addNumbers(numA, numB, numC, numD, numE) {
	console.log(numA + numB + numC + numD + numE);
}

// NaN: Not a Number
addNumbers(1,2,3,4,5); //15

// let's create a function that will generate a person's full name
function createFullName(fName, mName, lName) {
	console.log(lName + ', ' + fName + ' ' + mName);
}

// invoke the function and pass down the arguments needed that will take the place of each parameter
createFullName('Renei Katherine', 'Guno', 'Tabucol');

// upon using multiple arguments, it will correspond to the number of "parameters" declared in a funcion in succeeding order. Unless reordered inside the function.


// ----- SECTION 5 -----
// create a function that will display the stats of a pokemon in battle

let attackA = 'Tackle';
let attackB = 'Thunderbolt';
let attackC = 'Quick Attack';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack) {
	console.log(name + ' use ' + attack);
}

pokemonStats(selectedPokemon, attackC);
/*
	!NOTE: Using variables as arguments will allow us to utilize code reusability, this will help us reduce the amount of code that we have to write down.
*/

/*
	Return Statement
		- allows the output of a function to be passed on the line/block of code that called the function.
*/
// let's create a function that will return a string message
function returnString(){
	return 'Hello World!';
	2 + 1;
}

// lets repackage the result of this function inside a new variable
console.log(returnString());

// let's create a simple function to demonstrate the use and behavior of the return statement again
function dialog() {
	console.log('Oops! I did it again.');
	console.log("Don't you know that you're toxic.");
	return 'Im a slave for you!';
	console.log('Sometimes I run!');
}

// !NOTE: Any block/line of code that will come after our "return" statement is going to be ignored because it came after the end of the function execution.
console.log(dialog());

// The MAIN PURPOSE of the return statement inside a function is to identify which will be the FINAL OUTPUT/RESULT of the function and at the same time, determines the end of the function statement.


// ----- SECTION 7 -----
/*
	=> Function Parameters - can also accept other functions as their argument
	=> Some complex functions use other functions as their arguments to perform more complicated/complex results
*/
// we're going to create a simple function to be used as an argument later
function argumentToFunction() {
	console.log('This function was passed as an argument');
}

function invokeFunction(argumentToFunction, secondFunction) {
	argumentToFunction();
	secondFunction(selectedPokemon, attackB);
	secondFunction(selectedPokemon, attackA);
}

invokeFunction(argumentToFunction, pokemonStats);

// If you WANT to see the details/information about a function
console.log(invokeFunction);